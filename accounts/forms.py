from django import forms


# LOGIN
class LoginForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput(),
    )


# SIGNUP
class SignUpForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput(),
    )
    password_confirmation = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput(),
    )
