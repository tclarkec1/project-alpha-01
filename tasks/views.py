from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from tasks.form import CreateTaskForm
from tasks.models import Task


# Create your views here.
@login_required
def create_task(request):
    if request.method == "POST":
        form = CreateTaskForm(request.POST)
        if form.is_valid():
            task = form.save()
            task.owner = request.user
            task.save()
            return redirect("list_projects")
    else:
        form = CreateTaskForm()

    context = {
        "form": form,
    }

    return render(request, "tasks/create.html", context)


# show the tasks
@login_required
def show_my_tasks(request):
    mytasks = Task.objects.filter(assignee=request.user)
    context = {
        "mytasks": mytasks,
    }
    return render(request, "tasks/mine.html", context)
